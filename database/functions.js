const db = require('./index');
const moment = require('moment');

async function getEmpresaByToken(token) {
    if (!token) return false;

    const now = moment.utc().format('YYYY-MM-DDTHH:mm:ss');

    const login = await db('login_tokens')
        .where('token', token)
        .where('validade', '>=', now)
        .first();

    if (!login) return false;

    const empresa = await db('empresas')
        .where('id', login.empresaId)
        .where('ativo', true)
        .select()
        .first();

    if (!empresa) return false;

    return empresa;
}

module.exports = {
    getEmpresaByToken
}