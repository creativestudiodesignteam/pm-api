const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');

const routes = require('./routes/index.js');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static(__dirname));
app.set('view engine', 'ejs');
app.use(fileUpload());
app.use(routes);

app.listen(3003, () => {
  console.log('Servidor iniciado.')
});