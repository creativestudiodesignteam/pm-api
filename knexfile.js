// Update with your config settings.knex migrate:make migration_name 

module.exports = {

  production: {
    client: 'mysql',
    connection: {
      host: 'localhost',
      database: 'admin_frete',
      user: 'root',
      password: ''
    },
    useNullAsDefault: true,
  }

};
