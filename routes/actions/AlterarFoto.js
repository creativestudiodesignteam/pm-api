const db = require('../../database');
const { getEmpresaByToken } = require('../../database/functions');

module.exports = async (req, res) => {
    const { foto } = req.files;
    if (!foto) return res.status(400).send('Falha no envio da imagem.');

    try {
        const empresa = await getEmpresaByToken(req.headers.authorization);
        if (!empresa) return res.status(401).send('Falha na autenticação.');

        const fileExt = foto.name.split('.').pop();
        const filesAllowed = ['jpg', 'png'];
        const maxSize = 2097152;

        if (filesAllowed.indexOf(fileExt) === -1) {
            return res.status(400).send('Apenas arquivos JPG e PNG são permitidos.');
        }

        if (foto.size > maxSize) {
            return res.status(400).send('Tamanho máximo excedido, a imagem deve ter no máximo 2MB.');
        }

        await db('empresas').where('id', empresa.id).update({
            foto: foto.data,
            fotoTipo: fileExt.toLowerCase()
        });

        return res.send('Foto alterada com sucesso!');
    } catch (error) {
        console.log(error);
        return res.status(500).send('Não foi possível alterar a imagem');
    }
}