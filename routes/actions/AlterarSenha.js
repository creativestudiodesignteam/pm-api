const moment = require('moment');
const hmacSHA256 = require('crypto-js/hmac-sha256');

const config = require('../../config');
const db = require('../../database');
const { getEmpresaByToken } = require('../../database/functions');

module.exports = async (req, res) => {
    const {
        senhaAtual,
        novaSenha
    } = req.body;

    try {
        const empresa = await getEmpresaByToken(req.headers.authorization);
        if (!empresa) return res.status(401).send('Falha na autenticação.');

        if (novaSenha.length < 8)
            return res.status(400).send('A senha deve conter pelo menos 8 caracteres.');

        const hashedPassword = hmacSHA256(senhaAtual, empresa.cnpj + config.cryptoSecret).toString();

        if (empresa.senha !== hashedPassword)
            return res.status(400).send('Senha atual inválida.');

        const newHashedPassword = hmacSHA256(novaSenha, empresa.cnpj + config.cryptoSecret).toString();

        await db('empresas').where('id', empresa.id).update({
            senha: newHashedPassword
        });

        return res.send('Senha alterada com sucesso!');
    } catch (error) {
        console.log(error);
        return res.status(500).send('Não foi possível alterar a senha');
    }
}