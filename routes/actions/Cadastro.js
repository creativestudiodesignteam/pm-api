const moment = require('moment');
const hmacSHA256 = require('crypto-js/hmac-sha256');

const config = require('../../config');
const db = require('../../database');
const { validarCNPJ } = require('../../functions');

module.exports = async (req, res) => {
    const {
        razaoSocial,
        nomeFantasia,
        cnpj,
        inscricaoEstadual,
        cep,
        logradouro,
        numero,
        complemento,
        bairro,
        cidade,
        uf,
        email,
        telefone,
        precoPeso,
        precoCubagem,
        precoDistancia,
        entregaPessoaFisica,
        senha
    } = req.body;

    //Verifica os campos requeridos
    if (!(
        razaoSocial &&
        nomeFantasia &&
        cnpj &&
        inscricaoEstadual &&
        cep &&
        logradouro &&
        bairro &&
        cidade &&
        uf &&
        email &&
        telefone &&
        precoPeso &&
        precoCubagem &&
        precoDistancia &&
        (entregaPessoaFisica !== undefined) &&
        senha
    )) {
        return res.status(400).send('Preencha todos os campos.');
    }

    const nCnpj = cnpj.replace(/[^\d]+/g, '');
    const nCep = cep.replace(/[^\d]+/g, '');
    const nTelefone = telefone.replace(/[^\d]+/g, '');

    //Valida o CNPJ
    if (!validarCNPJ(nCnpj))
        return res.status(400).send('CNPJ inválido.');

    //Valida o CEP
    if (nCep.length !== 8) {
        return res.status(400).send('CEP inválido.');
    }

    //Valida o Telefone
    if (nTelefone.length < 10) {
        return res.status(400).send('Telefone inválido.');
    }

    //Valida a Senha
    if (senha.length < 8) {
        return res.status(400).send('A senha deve conter pelo menos 8 caracteres.');
    }

    try {
        const empresa = await db('empresas').select('*').where('cnpj', nCnpj).first();

        if (empresa) {
            return res.status(400).send('Esse CNPJ já possuí cadastro.');
        }

        const hashedPassword = hmacSHA256(senha, nCnpj + config.cryptoSecret).toString();
        const now = moment.utc().format('YYYY-MM-DDTHH:mm:ss');

        await db('empresas').insert({
            razaoSocial,
            nomeFantasia,
            cnpj: nCnpj,
            inscricaoEstadual,
            cep: nCep,
            logradouro,
            numero,
            complemento,
            bairro,
            cidade,
            uf,
            email,
            telefone: nTelefone,
            precoPeso,
            precoCubagem,
            precoDistancia,
            entregaPessoaFisica,
            senha: hashedPassword,
            criadoEm: now,
            ativo: true
        });

        res.send('Cadastro realizado com sucesso!');
    } catch (error) {
        console.log(error);
        res.status(500).send('Não foi possível realizar o cadastro.');
    }
}