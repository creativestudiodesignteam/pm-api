const { getEmpresaByToken } = require('../../database/functions');

module.exports = async (req, res) => {
    try {
        const empresa = await getEmpresaByToken(req.headers.authorization);
        if (!empresa) return res.status(401).send('Falha na autenticação.');

        delete (empresa.senha);
        return res.json({ ...empresa });
    } catch (error) {
        console.log(error);
        return res.status(500).send('Não foi possível realizar a autenticação');
    }
}