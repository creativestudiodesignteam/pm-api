const moment = require('moment');

const db = require('../../database');
const { getEmpresaByToken } = require('../../database/functions');

module.exports = async (req, res) => {
    const {
        razaoSocial,
        nomeFantasia,
        inscricaoEstadual,
        cep,
        logradouro,
        numero,
        complemento,
        bairro,
        cidade,
        uf,
        email,
        telefone,
    } = req.body;

    try {
        const empresa = await getEmpresaByToken(req.headers.authorization);
        if (!empresa) return res.status(401).send('Falha na autenticação.');

        if (!(
            razaoSocial &&
            nomeFantasia &&
            inscricaoEstadual &&
            cep &&
            logradouro &&
            bairro &&
            cidade &&
            uf &&
            email &&
            telefone
        )) {
            return res.status(400).send('Preencha todos os campos.');
        }

        const nCep = cep.replace(/[^\d]+/g, '');
        const nTelefone = telefone.replace(/[^\d]+/g, '');

        //Valida o CEP
        if (nCep.length !== 8) {
            return res.status(400).send('CEP inválido.');
        }

        //Valida o Telefone
        if (nTelefone.length < 10) {
            return res.status(400).send('Telefone inválido.');
        }

        await db('empresas').where('id', empresa.id).update({
            razaoSocial,
            nomeFantasia,
            inscricaoEstadual,
            cep: nCep,
            logradouro,
            numero,
            complemento,
            bairro,
            cidade,
            uf,
            email,
            telefone: nTelefone
        });

        return res.send('Dados alterados com sucesso!');
    } catch (error) {
        console.log(error);
        return res.status(500).send('Não foi possível alterar os dados');
    }
}