const moment = require('moment');

const db = require('../../database');
const { getEmpresaByToken } = require('../../database/functions');

module.exports = async (req, res) => {
    const {
        precoPeso,
        precoCubagem,
        precoDistancia,
        entregaPessoaFisica,
    } = req.body;

    try {
        const empresa = await getEmpresaByToken(req.headers.authorization);
        if (!empresa) return res.status(401).send('Falha na autenticação.');

        if (!(
            precoPeso &&
            precoCubagem &&
            precoDistancia &&
            (entregaPessoaFisica !== undefined)
        )) {
            return res.status(400).send('Preencha todos os campos.');
        }

        await db('empresas').where('id', empresa.id).update({
            precoPeso,
            precoCubagem,
            precoDistancia,
            entregaPessoaFisica: Boolean(entregaPessoaFisica)
        });

        return res.send('Dados alterados com sucesso!');
    } catch (error) {
        console.log(error);
        return res.status(500).send('Não foi possível alterar os dados');
    }
}