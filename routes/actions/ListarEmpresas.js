const db = require('../../database');

module.exports = async (req, res) => {
    try {
        const resultado = await db('empresas').select('*');
        const empresas = resultado.map(empresa => {
            delete empresa.senha;
            delete empresa.foto;
            delete empresa.fotoTipo;
            return empresa;
        });

        return res.json(empresas);
    } catch (error) {
        console.log(error);
        return res.status(500).send('Não foi possível consultar as empresas');
    }
}