const moment = require('moment');
const hmacSHA256 = require('crypto-js/hmac-sha256');

const config = require('../../config');
const db = require('../../database');

module.exports = async (req, res) => {
    const { cnpj, senha } = req.body;

    const nCnpj = cnpj.replace(/[^\d]+/g, '');
    const hashedPassword = hmacSHA256(senha, nCnpj + config.cryptoSecret).toString();

    try {
        const empresa = await db('empresas')
            .where('cnpj', nCnpj)
            .where('senha', hashedPassword)
            .first();

        if (!empresa) {
            console.log({ nCnpj, hashedPassword });
            return res.status(404).send('CNPJ ou senha inválida, tente novamente.');
        }

        const token = hmacSHA256(nCnpj + Date.now(), config.cryptoSecret).toString();
        const now = moment.utc().format('YYYY-MM-DDTHH:mm:ss');
        const validate = moment.utc().add(3, 'hours').format('YYYY-MM-DDTHH:mm:ss');

        await db('login_tokens').insert({
            token,
            empresaId: empresa.id,
            ip: req.ip,
            validade: validate,
            criadoEm: now,
        });

        return res.json({ token });
    } catch (err) {
        console.error(err);
        return res.status(500).send('Não foi possível realizar a autenticação.');
    }
}