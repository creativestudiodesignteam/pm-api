const rCadastro = require('./actions/Cadastro');
const rLogin = require('./actions/Login');
const rConsultarEmpresa = require('./actions/ConsultarEmpresa');
const rEditarDados = require('./actions/EditarDados');
const rEditarFrete = require('./actions/EditarFrete');
const rAlterarSenha = require('./actions/AlterarSenha');
const rAlterarFoto = require('./actions/AlterarFoto');
const rListarEmpresas = require('./actions/ListarEmpresas');

const express = require('express');
const routes = express.Router();

routes.get('/', (req, res) => res.status(200).send());
routes.post('/cadastro', rCadastro);
routes.post('/login', rLogin);
routes.get('/empresa', rConsultarEmpresa);
routes.post('/empresa/editar', rEditarDados);
routes.post('/empresa/editar/frete', rEditarFrete);
routes.post('/empresa/alterar_senha', rAlterarSenha);
routes.post('/empresa/alterar_foto', rAlterarFoto);
routes.get('/empresas', rListarEmpresas);

module.exports = routes;